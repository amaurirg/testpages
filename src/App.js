import React, { Component, Fragment } from 'react';

import './assets/fonts.css';
import styles from './StyleApp';
import { withStyles, ThemeProvider } from "@material-ui/styles";
import { CssBaseline, 
         MuiThemeProvider, 
         createMuiTheme } from '@material-ui/core';

import Layout from './components/Layout/Layout';

const theme = createMuiTheme({
  typography: {
    fontFamily: '"Titillium Web", sans-serif',
    fontStyle: 'normal',
    fontDisplay: 'swap',
    fontWeight: 400,
    color: 'white',
    subtitle1: {
      fontSize: 20,
    }
  },
  overrides: {
    MuiSelect: {
      select: {
        "&:focus": {
          width: '100%',
          height: '2.5rem',
          backgroundColor: "white",
          borderRadius: '12rem',
          padding: '0px 2.35rem 0px 1rem',
          margin: '0px',
        },
        color: 'white'
      }
    },
  }
})

class App extends Component {
  render() {

    return (
      <Fragment>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <MuiThemeProvider theme={theme}>
            <Layout>
            </Layout>
          </MuiThemeProvider>
        </ThemeProvider>
      </Fragment>
    );
  }
}

export default withStyles(styles)(App);