const styles = theme => ({
    divider: {
        width: '100%',
        maxWidth: 90,
        backgroundColor: '#E13D31',
        height: 3
    },
    circularProgress: {
        color: '#E13D31',
        marginLeft: '39%',
    }
});

export default styles;