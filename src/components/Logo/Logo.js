import React from 'react'

const Logo = ({ title, ...props }) => (
  <svg
    id="prefix__Layer_1"
    x={0}
    y={0}
    viewBox="0 0 314.92 45.82"
    xmlSpace="preserve"
    width="6em"
    height="4em"
    {...props}
  >
    {title ? <title>{title}</title> : null}
    <style>{'.prefix__st0{fill:none}'}</style>
    <path className="prefix__st0" d="M-437.1-502.34H753.45v841.89H-437.1z" />
    <path className="prefix__st0" d="M-437.1-502.34H753.45v841.89H-437.1z" />
    <path d="M276.47.12c13.12 0 25.96 1.14 38.45 3.32l-1.72 9.36a212.783 212.783 0 00-31.67-3.12v35.31h-10.31V9.69c-10.84.26-21.24 1.31-31.63 
    3.14l-1.72-9.36c12.56-2.21 25.41-3.35 38.6-3.35M104.08 1.77H93.75v43.31h10.33V1.77zM1.43 3.56l1.72 9.35c10.39-1.83 20.78-2.88 
    31.63-3.14v35.31h10.31V9.77c10.81.26 21.31 1.31 31.66 3.12l1.73-9.36C65.98 1.34 53.15.21 40.03.21c-13.19 0-26.05 1.15-38.6 
    3.35m210.85 41.52h10.32V1.77h-10.32v43.31zm-54.09-12.45C146.55 22.11 136.13 10 128.88 0l-8.02 5.85c8.54 12.44 24.45 29.27 37.33 
    39.97l.09-.08c12.86-10.7 28.72-27.48 37.25-39.89L187.51 0c-7.25 10-17.66 22.11-29.32 32.63" fill="currentColor"/>
    <path className="prefix__st0" d="M-437.1-502.34H753.45v841.89H-437.1z" />
    <path className="prefix__st0" d="M-437.1-502.34H753.45v841.89H-437.1z" />
  </svg>
)

export default Logo;