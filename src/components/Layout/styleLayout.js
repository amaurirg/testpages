const styles = theme => ({
  root: {
    display: 'flex',
  },
  content: {
    flexGrow: 1,
    marginTop: theme.spacing(9),
    padding: theme.spacing(3),
    [theme.breakpoints.only('xs')]: {
      padding: 0,
      backgroundColor: 'green'
    },
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
  },
})
export default styles;