import React, { Component, Fragment } from 'react';

import Toolbar from '../Navigation/Toolbar/Toolbar';
import SideDrawer from '../Navigation//SideDrawer/SideDrawer';
import { withStyles } from '@material-ui/styles';

import styles from './styleLayout';

class Layout extends Component {
    state = {
      open: false
    };
    
    handleDrawerOpen = () => {    
      this.setState({open: true})
    };
    
    handleDrawerClose = () => {
      this.setState({open: false})
    };

    render(){
      const { classes, theme, children } = this.props;
      const { open } = this.state;

      return (
        <Fragment>
        {/*<div>Toolbar, SideDrawer, Backdrop</div>*/}
          <div className={classes.root}>
            <Toolbar handleDrawerOpen={this.handleDrawerOpen} className={classes.toolbar}/>
            <SideDrawer open={open} classes={classes} theme={theme} handleDrawerClose={this.handleDrawerClose} />
            <main className={classes.content}>
              {children}
            </main>
          </div>
        </Fragment>
      )
    };
};

export default withStyles(styles)(Layout);