const drawerWidth = 240;

const styles = theme => ({
    root: {
        display: 'flex',
        shadow: 'none',
      },
      appBar: {
        backgroundColor: '#E13D31',
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen,
        }),
      },
      appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.enteringScreen,
        }),
      },
      toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        position: 'relative',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
      },
      menuButton: {
        marginRight: 36,
      },
      hide: {
        display: 'none',
      },
      logo: {
        position: 'absolute',
        left: '50%',
        marginLeft: '-50px',
        display:'block'
      },
      brand: {
        width: '150%',
        color: 'white',
      },
      appsButton: {
        position: 'absolute',
        left: '95%',
        marginRight: 0,
      }
});

export default styles;