import React, { Component, Fragment } from "react";
import Logo from "../../Logo/Logo";
import clsx from "clsx";
import { withStyles } from "@material-ui/core/styles";
import { AppBar, CssBaseline, IconButton, Toolbar } from "@material-ui/core";
import { Menu } from "@material-ui/icons";
import styles from "./StyleToolbar";


class toolbar extends Component {
  render() {
    const { classes, open, handleDrawerOpen } = this.props;
    return (
      <Fragment>
        <div className={classes.root}>
          <CssBaseline />
          <div className={classes.toolbar}>
            <AppBar
              position="fixed"
              elevation={0}
              className={clsx(classes.appBar, {
                [classes.appBarShift]: open
              })}
            >
              <Toolbar>
                <IconButton
                  color="inherit"
                  aria-label="open drawer"
                  onClick={handleDrawerOpen}
                  edge="start"
                  className={clsx(classes.menuButton, {
                    [classes.hide]: open
                  })}
                >
                  <Menu />
                </IconButton>
                <div className={classes.logo}>
                  <Logo className={classes.brand} />
                </div>
              </Toolbar>
            </AppBar>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default withStyles(styles)(toolbar);
