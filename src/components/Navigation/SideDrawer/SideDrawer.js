import React, { Fragment } from "react";

import LogoDrawer from "../../Logo/LogoDrawer";
import clsx from "clsx";
import {
  Avatar,
  Drawer,
  IconButton,
  List,
  ListItem,
  ListItemAvatar,
  ListItemIcon,
  ListItemText
} from "@material-ui/core";
import { Close, Home, Sync, ExitToApp } from "@material-ui/icons";
import { withStyles } from "@material-ui/styles";
import styles from "./StyleSideDrawer";

import { useTheme } from "@material-ui/core/styles";
import useMediaQuery from "@material-ui/core/useMediaQuery";


function SideDrawer(props) {
  const { classes, open, handleDrawerClose } = props;

  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.up("sm"));

  const drawer = (
    <div>
      <div className={classes.toolbar}>
        <div className={classes.logoDrawer}>
          <LogoDrawer className={classes.brandDrawer} />
        </div>
        <IconButton onClick={handleDrawerClose}>
          <Close />
        </IconButton>
      </div>
      <List>
        <ListItem>
          <ListItemAvatar>
            <Avatar alt="Foto_Login" img="../../../assets/images/profilepicturev2.jpg" />
          </ListItemAvatar>
        </ListItem>
      </List>
      <List>
        {["Home", "Item 1"].map((text, index) => (
          <ListItem
            button
            key={text}
            className={classes.listItem}
            onClick={handleDrawerClose}
          >
            <ListItemIcon className={classes.icon}>
              {index % 2 === 0 ? <Home /> : <Sync />}
            </ListItemIcon>
            <ListItemText primary={text} />
          </ListItem>
        ))}
      </List>

      <List>
        <ListItem button key="Sair da Conta" className={classes.listItem}>
          <ListItemIcon className={classes.icon}>
            <ExitToApp />
          </ListItemIcon>
          <ListItemText primary="Sair da Conta" />
        </ListItem>
      </List>
    </div>
  );
  return (
    <Fragment>
      <div className={classes.root}>
        <Drawer
          variant={isMobile ? "permanent" : "temporary"}
          className={clsx(classes.drawer, {
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open
          })}
          classes={{
            paper: clsx({
              [classes.drawerOpen]: open,
              [classes.drawerClose]: !open
            })
          }}
          open={open}
        >
          {drawer}
        </Drawer>
      </div>
    </Fragment>
  );
}

export default withStyles(styles)(SideDrawer);